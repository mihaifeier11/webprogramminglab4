document: onload = function () {

}

function validate() {
    let name = document.getElementById("name");
    let dob = document.getElementById("birthdate");
    let age = document.getElementById("age");
    let email = document.getElementById("email");
    let completedCorectly = true;

    if (!validateName(name.value)) {
        name.style.border = "1px solid red";
        completedCorectly = false;
    }

    let ageFromDOB = calculateAge(dob.value);
    if (age.value != ageFromDOB) {
        dob.style.border = "1px solid red";
        age.style.border = "1px solid red";
        completedCorectly = false;
    }

    if (!validateEmail(email.value)) {
        email.style.border = "1px solid red";
        completedCorectly = false;
    }

    if (completedCorectly) {
        alert("Formularul a fost completat corect");
    }


    function validateName(name) {
        if (!/^[a-zA-Z]+$/.test(name)) {
            return false;
        }

        return true;
    }

    function calculateAge(dob) {
        dateOfBirth = new Date(dob);
        console.log(dob);
        let differenceMs = Date.now() - dateOfBirth.getTime();
        let age = new Date(differenceMs);

        return Math.abs(age.getUTCFullYear() - 1970);
    }

    function validateEmail(mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return true;
        }
        return false;
    }
}