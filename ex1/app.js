document: onload = function () {
    let select1 = document.getElementById("select1");
    let select2 = document.getElementById("select2");

    select1.addEventListener("change", (event) => {
        let selectedOption = select1.options[select1.selectedIndex];
        select2.appendChild(selectedOption);
        console.log(selectedOption);
    });
    select2.addEventListener("change", (event) => {
        let selectedOption = select2.options[select2.selectedIndex];
        select1.appendChild(selectedOption);
        console.log(selectedOption);
    });

}